'use strict';

describe('Controller: TopbarController', function () {
    // load the controller's module
    beforeEach(module('jBlogApp'));

    var TopbarCtrl, scope;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, $rootScope) {
        scope = $rootScope.$new();
        TopbarCtrl = $controller('TopbarController', {
            $scope: scope
        });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
        // TODO: Test controller
    });
});
