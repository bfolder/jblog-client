'use strict';

describe('Controller: SidebarController', function () {
    // load the controller's module
    beforeEach(module('jBlogApp'));

    var SidebarCtrl, scope, httpBackend;

    // Initialize the controller, a mock scope and an httpBackend
    beforeEach(inject(function ($controller, $rootScope, $httpBackend) {
        scope = $rootScope.$new();
        httpBackend = $httpBackend;
        SidebarCtrl = $controller('SidebarController', {
            $scope: scope
        });
    }));

    it('should retrieve a list of categories', function () {
        // Mock Data
        httpBackend.whenGET(baseURL + "/rest/categories").respond([
            {
                "name": "Category",
                "entries": [
                    {
                        "dateCreated": 12321321,
                        "comments": [
                            {
                                "entry": {"id": 0},
                                "authorEmail": "author@author.com",
                                "id": 0,
                                "text": "Comment",
                                "authorName": "Author"
                            }
                        ],
                        "id": 0,
                        "text": "Entry",
                        "category": {"id": 0},
                        "title": "Entry title",
                        "dateUpdated": 12344444
                    }
                ],
                "id": 0
            },
            {
                "name": "Second Category",
                "entries": [
                    {
                        "dateCreated": 123123123,
                        "comments": [
                            {
                                "entry": {"id": 1},
                                "authorEmail": "author@author.com",
                                "id": 1,
                                "text": "Second Comment",
                                "authorName": "Author"
                            }
                        ],
                        "id": 1,
                        "text": "Second Entry",
                        "category": {"id": 1},
                        "title": "Entry title",
                        "dateUpdated": 12344444
                    }
                ],
                "id": 1
            }
        ]);
        scope.categories.$promise.then(function (data) {
            expect(data[0].name).toEqual("Category");
            expect(data[0].id).toEqual(0);
            expect(data[1].name).toEqual("Second Category");
            expect(data[1].id).toEqual(1);
        });
        httpBackend.flush();
    });
});
