'use strict';

describe('Controller: EntryListController', function () {
    // load the controller's module
    beforeEach(module('jBlogApp'));

    var EntryCtrl, scope, httpBackend;

    // Initialize the controller, a mock scope and an httpBackend
    beforeEach(inject(function ($controller, $rootScope, $httpBackend) {
        scope = $rootScope.$new();
        httpBackend = $httpBackend;
        EntryCtrl = $controller('EntryListController', {
            $scope: scope
        });
    }));

    it('should attach a list of awesomeThings to the scope', function () {
        // TODO: Test controller logic here
    });
});

describe('Controller: EntryViewController', function () {
    // load the controller's module
    beforeEach(module('jBlogApp'));

    var EntryCtrl, scope, httpBackend;

    // Initialize the controller, a mock scope and an httpBackend
    beforeEach(inject(function ($controller, $rootScope, $httpBackend) {
        scope = $rootScope.$new();
        httpBackend = $httpBackend;
        EntryCtrl = $controller('EntryViewController', {
            $scope: scope
        });
    }));

    it('should retrieve a single entry', function () {
        // Mock Data
        httpBackend.whenGET(baseURL + "/rest/entries").respond(
            {
                "dateCreated": 12321321,
                "comments": [
                    {
                        "entry": {"id": 0},
                        "authorEmail": "author@author.com",
                        "id": 0,
                        "text": "Comment",
                        "authorName": "Author"
                    }
                ],
                "id": 0,
                "text": "Entry",
                "category": {"id": 0},
                "title": "Entry title",
                "dateUpdated": 12344444
            });

        scope.entry.$promise.then(function (data) {
            expect(data.text).toEqual("Entry");
            expect(data.title).toEqual("Entry title");
            expect(data.dateCreated).toEqual(12321321);
            expect(data.dateUpdated).toEqual(12344444);
            expect(data.id).toEqual(0);
            expect(data.comments[0].entry.id).toEqual(0);
            expect(data.comments[0].text).toEqual("Comment");
        });
        httpBackend.flush();
    });
});
