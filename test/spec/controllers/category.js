'use strict';

describe('Controller: CategoryViewController', function () {
    // load the controller's module
    beforeEach(module('jBlogApp'));

    var CategoryCtrl, scope, httpBackend;

    // Initialize the controller, a mock scope and an httpBackend
    beforeEach(inject(function ($controller, $rootScope, $httpBackend) {
        scope = $rootScope.$new();
        httpBackend = $httpBackend;
        CategoryCtrl = $controller('CategoryViewController', {
            $scope: scope
        });
    }));

    it('should retrieve a single category', function () {
        // Mock Data
        httpBackend.whenGET(baseURL + "/rest/categories").respond(
            {
                "name": "Category",
                "entries": [
                    {
                        "dateCreated": 12321321,
                        "comments": [
                            {
                                "entry": {"id": 0},
                                "authorEmail": "author@author.com",
                                "id": 0,
                                "text": "Comment",
                                "authorName": "Author"
                            }
                        ],
                        "id": 0,
                        "text": "Entry",
                        "category": {"id": 0},
                        "title": "Entry title",
                        "dateUpdated": 12344444
                    }
                ],
                "id": 0
            });


        scope.category.$promise.then(function (data) {
            expect(data.name).toEqual("Category");
            expect(data.id).toEqual(0);
            expect(data.entries.length).toEqual(1);
        });
        httpBackend.flush();
    });
});
