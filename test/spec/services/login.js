'use strict';

describe('Service: Login', function () {
    // load the service's module
    beforeEach(module('jBlogApp'));

    // instantiate service
    var login;
    beforeEach(inject(function (_LoginService_) {
        login = _LoginService_;
    }));

    it('should get the login resource promise', function () {
        expect(login).toBeDefined();
        expect(login.authenticate()).toBeDefined();
    });
});