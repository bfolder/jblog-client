'use strict';

describe('Service: Comment', function () {
    // load the service's module
    beforeEach(module('jBlogApp'));

    // instantiate service
    var comment;
    beforeEach(inject(function (_Comment_) {
        comment = _Comment_;
    }));

    it('should get the comment resource promise', function () {
        expect(comment).toBeDefined();
        expect(comment.get()).toBeDefined();
        expect(comment.query()).toBeDefined();
        expect(comment.save()).toBeDefined();
    });

});
