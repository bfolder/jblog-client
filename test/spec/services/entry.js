'use strict';

describe('Service: Entry', function () {
    // load the service's module
    beforeEach(module('jBlogApp'));

    // instantiate service
    var entry;
    beforeEach(inject(function (_Entry_) {
        entry = _Entry_;
    }));

    it('should get the entry resource promise', function () {
        expect(entry).toBeDefined();
        expect(entry.get()).toBeDefined();
        expect(entry.query()).toBeDefined();
        expect(entry.save()).toBeDefined();
    });
});
