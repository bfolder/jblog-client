'use strict';

describe('Service: Category', function () {
    // load the service's module
    beforeEach(module('jBlogApp'));

    // instantiate service
    var category;
    beforeEach(inject(function (_Category_) {
        category = _Category_;
    }));

    it('should get the category resource promise', function () {
        expect(category).toBeDefined();
        expect(category.get()).toBeDefined();
        expect(category.query()).toBeDefined();
        expect(category.save()).toBeDefined();
    });
});
