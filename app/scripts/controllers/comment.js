'use strict';

/**
 * @ngdoc function
 * @name jBlogApp.controller:CategoryController
 * @description
 * # CategoryController
 * Controller of the jBlogApp
 */
angular.module('jBlogApp')
    .controller('CommentController', function ($scope, $route, Comment) {
        $scope.comment = new Comment();
        $scope.addComment = function() {
            $scope.comment.entry = $scope.entry;
            Comment.save($scope.comment, function() {
                $route.reload(); // Reload data and route
            });
        }
    });
