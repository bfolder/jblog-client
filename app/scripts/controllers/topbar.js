'use strict';

/**
 * @ngdoc function
 * @name jBlogApp.controller:TopbarController
 * @description
 * # TopbarController
 * Controller of the jBlogApp
 */
angular.module('jBlogApp')
    .controller('TopbarController', function ($scope, $rootScope, EntryService, CategoryService, $modal) {
        $scope.addEntry = EntryService.addOrEdit;
        $scope.addCategory = CategoryService.addOrEdit;
        $scope.showLogin = function () {
            $rootScope.modalInstance = $modal.open({
                templateUrl: 'views/login.html',
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $modalInstance) {
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        }
    });
