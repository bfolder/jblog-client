'use strict';

/**
 * @ngdoc function
 * @name jBlogApp.controller:EntryListController
 * @description
 * # EntryListController
 * Controller of the jBlogApp
 */
angular.module('jBlogApp')
    .controller('EntryListController', function ($scope, Entity, Entry, EntryService) {
        $scope.entries = Entry.query();
        $scope.editEntry = EntryService.addOrEdit;
        $scope.deleteEntry = Entity.delete;
    });

/**
 * @ngdoc function
 * @name jBlogApp.controller:EntryViewController
 * @description
 * # EntryViewController
 * Controller of the jBlogApp
 */
angular.module('jBlogApp')
    .controller('EntryViewController', function ($scope, $log, $routeParams, $location, Entry, Entity, EntryService) {
        $scope.entry = Entry.get({id: $routeParams.id}, function () {
        }, function (error) {
            $location.path("/").replace(); // Redirect to base path if there was an error
        });
        $scope.editEntry = EntryService.addOrEdit;
        $scope.deleteEntry = Entity.delete;
    });
