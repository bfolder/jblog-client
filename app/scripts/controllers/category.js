'use strict';

/**
 * @ngdoc function
 * @name jBlogApp.controller:CategoryViewController
 * @description
 * # CategoryViewController
 * Controller of the jBlogApp
 */
angular.module('jBlogApp')
    .controller('CategoryViewController', function ($scope, $rootScope, $routeParams, $location, Category, CategoryService, Entity, EntryService) {
        $scope.category =  Category.get({id: $routeParams.id}, function (data) {
            // Do nothing
        }, function (error) {
            $location.path("/").replace(); // Redirect to base path if there was an error
        });

        $scope.editCategory = CategoryService.addOrEdit;
        $scope.deleteCategory = Entity.delete;
        $scope.editEntry = EntryService.addOrEdit;
        $scope.deleteEntry = Entity.delete;
    });
