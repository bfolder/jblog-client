'use strict';

/**
 * @ngdoc function
 * @name jBlogApp.controller:SidebarController
 * @description
 * # SidebarController
 * Controller of the jBlogApp
 */
angular.module('jBlogApp')
    .controller('SidebarController', function ($scope, Category) {
        $scope.categories = Category.query();

        $scope.$on('categoryReload', function () {
            $scope.categories = Category.query(); // Reload categories
        });
    });
