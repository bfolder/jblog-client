'use strict';

/**
 * @ngdoc function
 * @name jBlogApp.controller:LoginController
 * @description
 * # LoginController
 * Controller of the jBlogApp
 */
angular.module('jBlogApp')
    .controller('LoginController', function ($scope, $rootScope, $location, $http, $cookieStore, $injector, LoginService) {
        $scope.login = function() {
            LoginService.authenticate($.param({username: $scope.username, password: $scope.password}), function(user) {
                $rootScope.user = user;
                $http.defaults.headers.common['x-auth-token'] = user.token;
                $cookieStore.put('user', user);
                $location.path("/");
                var modalInstance = $rootScope.modalInstance;
                modalInstance.dismiss("close");
                delete $rootScope.modalInstance;
            }, function(httpResponse) {
                $scope.invalid = (httpResponse.status == 403 || httpResponse.status == 401);
            });
        };
    });
