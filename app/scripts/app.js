'use strict';

/**
 * @ngdoc overview
 * @name jBlogApp
 * @description
 * # jBlogApp
 *
 * Main module of the application.
 */
angular
    .module('jBlogApp', [
        'angular-loading-bar',
        'ui.bootstrap',
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch'
    ])
    .config(function ($routeProvider, $httpProvider, cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = true;

        $routeProvider
            .when('/', {
                redirectTo: '/entries'
            })
            .when('/entries', {
                templateUrl: 'views/entry/entry_list.html',
                controller: 'EntryListController'
            })
            .when('/entries/:id', {
                templateUrl: 'views/entry/entry_view.html',
                controller: 'EntryViewController'
            })
            .when('/categories', {
                redirectTo: '/'
            })
            .when('/categories/:id', {
                templateUrl: 'views/category/category_view.html',
                controller: 'CategoryViewController'
            })
            .otherwise({
                redirectTo: '/'
            });
    }).run(function ($rootScope, $http, $location, $cookieStore, LoginService) {
        /* Define a global logout */
        $rootScope.logout = function () {
            delete $rootScope.user;
            delete $http.defaults.headers.common['x-auth-token'];
            $cookieStore.remove('user');
            $location.path("/");
        };

        /* Try getting valid user from cookie */
        var originalPath = $location.path();
        $location.path("/");
        var user = $cookieStore.get('user');
        if (user !== undefined) {
            $rootScope.user = user;
            $http.defaults.headers.common['x-auth-token'] = user.token;
            $location.path(originalPath);
        }
    });

var baseUrl = "http://localhost:8080";