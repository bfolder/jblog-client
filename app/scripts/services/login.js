'use strict';

/**
 * @ngdoc service
 * @name jBlogApp.LoginService
 * @description
 * # LoginService
 * Factory in the jBlogApp.
 */
angular.module('jBlogApp')
    .factory('LoginService', function ($resource) {
        return $resource(baseUrl + '/rest/authenticate', {},
            {
                authenticate: {
                    method: 'POST',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }
            }
        );
    });

