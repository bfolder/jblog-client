'use strict';

/**
 * @ngdoc service
 * @name jBlogApp.Category
 * @description
 * # Category
 * Factory in the jBlogApp.
 */
angular.module('jBlogApp')
    .factory('Category', function ($resource) {
        return $resource(baseUrl + '/rest/categories/:id');
    })
    .factory('CategoryService', function ($modal) {
        return {"addOrEdit": function (object) {
            $modal.open({
                templateUrl: 'views/category/category_edit.html',
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $rootScope, $modalInstance, $log, $route, Category) {
                    $scope.edit = object;
                    $scope.category = object ? object : new Category();
                    $scope.submit = function () {
                        $log.debug('Submitting category.');
                        $log.debug($scope.category);
                        Category.save($scope.category, function () {
                            $modalInstance.dismiss('cancel');
                            $rootScope.$broadcast('categoryReload'); // Reload category data in child controllers (e.g. sidebar)
                            $route.reload(); // Reload data and route
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        }}
    });
