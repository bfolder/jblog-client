'use strict';

/**
 * @ngdoc service
 * @name jBlogApp.Comment
 * @description
 * # Comment
 * Factory in the jBlogApp.
 */
angular.module('jBlogApp')
    .factory('Comment', function ($resource) {
        return $resource(baseUrl + '/rest/comments/:id');
    });