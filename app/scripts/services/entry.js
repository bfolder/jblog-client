'use strict';

/**
 * @ngdoc service
 * @name jBlogApp.Entry
 * @description
 * # Entry
 * Factory in the jBlogApp.
 */
angular.module('jBlogApp')
    .factory('Entry', function ($resource, $log) {
        $log.debug($resource);
        return $resource(baseUrl + '/rest/entries/:id')
    })
    .factory('EntryService', function ($modal) {
        return {"addOrEdit": function (object) {
            $modal.open({
                templateUrl: 'views/entry/entry_edit.html',
                backdrop: true,
                windowClass: 'modal',
                controller: function ($scope, $modalInstance, $log, $route, Entry, Category) {
                    $scope.edit = object;
                    $scope.entry = object ? object : new Entry();
                    $scope.categories = Category.query();
                    $scope.submit = function () {
                        $log.debug('Submitting entry.');
                        $log.debug($scope.entry);
                        Entry.save($scope.entry, function () {
                            $modalInstance.dismiss('cancel');
                            $route.reload(); // Reload data and route
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        }
        }
    });

